<!--
Get your module up and running quickly.

Find and replace all on all files (CMD+SHIFT+F):
- Name: Nuxt 3 Payload CMS
- Package name: nuxt-payloadcms
- Description: My new Nuxt module
-->

# Nuxt 3 Payload CMS Module

[![npm version][npm-version-src]][npm-version-href]
[![npm downloads][npm-downloads-src]][npm-downloads-href]
[![License][license-src]][license-href]
[![Nuxt][nuxt-src]][nuxt-href]

Nuxt 3 Module to integrate payloadcms apis

- [✨ &nbsp;Release Notes](/CHANGELOG.md)
<!-- - [🏀 Online playground](https://stackblitz.com/github/your-org/nuxt-payloadcms?file=playground%2Fapp.vue) -->
<!-- - [📖 &nbsp;Documentation](https://example.com) -->

## Features

<!-- Highlight some of the features your module provide here -->
- ⛰ &nbsp;Foo
- 🚠 &nbsp;Bar
- 🌲 &nbsp;Baz

## Quick Setup
Configure the `@altercode` registry in your `.npmrc` file

```bash
echo @altercode:registry=https://gitlab.com/api/v4/projects/48301831/packages/npm/ >> .npmrc
```

1. Add `@altercode/nuxt-payloadcms` dependency to your project

```bash
# Using pnpm
pnpm add -D @altercode/nuxt-payloadcms

# Using yarn
yarn add --dev @altercode/nuxt-payloadcms

# Using npm
npm install --save-dev @altercode/nuxt-payloadcms
```

2. Add `nuxt-payloadcms` to the `modules` section of `nuxt.config.ts`

```js
export default defineNuxtConfig({
  modules: [
    '@altercode/nuxt-payloadcms'
  ]
})
```

That's it! You can now use Nuxt 3 Payload CMS in your Nuxt app ✨

## Development

```bash
# Install dependencies
npm install

# Generate type stubs
npm run dev:prepare

# Develop with the playground
npm run dev

# Build the playground
npm run dev:build

# Run ESLint
npm run lint

# Run Vitest
npm run test
npm run test:watch

# Release new version
npm run release
```

<!-- Badges -->
[npm-version-src]: https://img.shields.io/npm/v/nuxt-payloadcms/latest.svg?style=flat&colorA=18181B&colorB=28CF8D
[npm-version-href]: https://npmjs.com/package/nuxt-payloadcms

[npm-downloads-src]: https://img.shields.io/npm/dm/nuxt-payloadcms.svg?style=flat&colorA=18181B&colorB=28CF8D
[npm-downloads-href]: https://npmjs.com/package/nuxt-payloadcms

[license-src]: https://img.shields.io/npm/l/nuxt-payloadcms.svg?style=flat&colorA=18181B&colorB=28CF8D
[license-href]: https://npmjs.com/package/nuxt-payloadcms

[nuxt-src]: https://img.shields.io/badge/Nuxt-18181B?logo=nuxt.js
[nuxt-href]: https://nuxt.com
