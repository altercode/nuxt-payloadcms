import {defineNuxtModule, addPlugin, createResolver, addImportsDir} from '@nuxt/kit'
import defu from "defu";
import type { CookieOptions } from 'nuxt/dist/app/composables/cookie'

// Module options TypeScript interface definition
export interface ModuleOptions {
  url?: string

  /**
   * Nuxt Cookie Options
   * @default {}
   * @type CookieOptions
   */
  cookie?: CookieOptions,

  cookieName:string
}

export default defineNuxtModule<ModuleOptions>({
  meta: {
    name: '@altercode/nuxt-payloadcms',
    configKey: 'payloadcms',
    compatibility: {
      // Semver version of supported nuxt versions
      nuxt: '^3.0.0'
    }
  },
  // Default configuration options of the Nuxt module
  defaults: {
    url:process.env.PAYLOADCMS_URL || 'http://localhost:4500',
    cookie:{},
    cookieName:'payload_jwt'
  },

  setup(options, nuxt) {
    const {resolve} = createResolver(import.meta.url)
    const runtimeDir = resolve('./runtime')
    // @ts-ignore
    nuxt.options.runtimeConfig.public.payloadcms = defu(nuxt.options.runtimeConfig.public.payloadcms, options)
    // @ts-ignore
    nuxt.options.runtimeConfig.payloadcms = defu(nuxt.options.runtimeConfig.payloadcms, options)
    // Do not add the extension since the `.ts` will be transpiled to `.mjs` after `npm run prepack`
    addPlugin(resolve(runtimeDir,'plugin'))
    addImportsDir(resolve(runtimeDir, 'composables'))
  }
})
