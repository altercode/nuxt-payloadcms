import { useCookie, useNuxtApp, useRuntimeConfig } from '#app'

export const usePayloadToken = () => {
  const nuxt = useNuxtApp()
  const config = process.server ? useRuntimeConfig() : useRuntimeConfig().public

  nuxt._cookies = nuxt._cookies || {}
  // @ts-ignore
  if (nuxt._cookies[config.payloadcms.cookieName]) {  // @ts-ignore

    return nuxt._cookies[config.payloadcms.cookieName]
  }

  const cookie = useCookie<string | null>(config.payloadcms.cookieName, config.payloadcms.cookie)
  // @ts-ignore

  nuxt._cookies[config.payloadcms.cookieName] = cookie
  return cookie
}
