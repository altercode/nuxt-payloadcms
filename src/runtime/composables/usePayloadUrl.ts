import { useRuntimeConfig } from '#app'

export const usePayloadUrl = (): string => {
  const config = process.server ? useRuntimeConfig() : useRuntimeConfig().public

  return config.payloadcms.url
}
