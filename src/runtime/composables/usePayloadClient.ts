import { stringify } from 'qs'
import {useNuxtApp, useRuntimeConfig} from '#app'
import { usePayloadToken } from './usePayloadToken'
import {usePayloadUrl} from "./usePayloadUrl";
import {FetchOptions} from "ofetch";
import {a} from "vite-node/types-516036fa";


export const usePayloadClient = () => {
  const nuxt = useNuxtApp()
  const token = usePayloadToken()
  const baseURL = usePayloadUrl()


  return async <T> (url: string, fetchOptions: FetchOptions|any = {}): Promise<T> => {
    const headers: HeadersInit = {}

    if (token && token.value) {
      headers.Authorization = `Bearer ${token.value}`
    }

    // Map params
    if (fetchOptions.params) {
      const params = stringify(fetchOptions.params, { encodeValuesOnly: true })
      if (params) {
        url = `${url}?${params}`
      }
      delete fetchOptions.params
    }

    try {
      return await $fetch<T>(url, {
        retry: 0,
        baseURL,
        ...fetchOptions,
        headers: {
          ...headers,
          ...fetchOptions.headers
        }
      })
    } catch (err: any) {
      const e = err.data

      nuxt.hooks.callHook('payloadcms:error' as any, e)
      throw e
    }
  }
}
