import { useRuntimeConfig } from '#app'
import { print } from 'graphql'
import type { DocumentNode } from 'graphql'

import {PayloadCMSGraphqlVariables} from "../types";
import {usePayloadClient} from "./usePayloadClient";



export const usePayloadGraphQL= ()=>  {
  const client = usePayloadClient()
  const config = process.server ? useRuntimeConfig() : useRuntimeConfig().public


  return <T> (query: string|DocumentNode, variables?: PayloadCMSGraphqlVariables): Promise<T> => {
    const queryAsString = typeof query === 'string' ? query : print(query)
    return client('/graphql', {
      method: 'POST',
      body: {
        query: queryAsString,
        variables
      },
      headers: {
        accept: 'application/json'
      },
      baseURL: config.payloadcms.url
    })
  }
}
