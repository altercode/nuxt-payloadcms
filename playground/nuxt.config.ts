export default defineNuxtConfig({
  modules: ['../src/module'],
  payloadcms: {
    url:'http://localhost:4500/api'
  },
  devtools: { enabled: true }
})
